ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "FileSearch",
    libraryDependencies ++= Seq(
      "com.github.pureconfig" %% "pureconfig" % "0.17.3",
      "org.typelevel" %% "cats-core" % "2.9.0",
      "co.fs2" %% "fs2-io" % "3.6.1",
      "co.fs2" %% "fs2-core" % "3.6.1",
      "org.scalatest" %% "scalatest" % "3.2.15" % Test,
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
      "ch.qos.logback" % "logback-classic" % "1.4.7"
    ),
    // https://github.com/sbt/sbt-assembly#merge-strategy
    assembly / assemblyMergeStrategy := {
      case PathList("module-info.class") => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    }
  )

addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")
