# File Search cli

An app to find files that contain a given word

---

### Setting up your machine

Run these commands to install the necessary binaries to run the app:

```shell
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

sdk install java 17.0.7-tem

sdk install scala 2.13.10

sdk install sbt 1.8.2
```

**To install docker follow the steps on https://docs.docker.com/engine/install/**

### Using the accompanying script:
A script is provided to define two shell functions, so you can type less to run the search.

Run `source` to make function definitions available:
```shell
source $(pwd)/scripts.sh
```
To run a search with sbt:
```shell
fs someword $HOME/Documents/
```
To run a search with docker:
```shell
fsd someword $HOME/Documents/
```
### How to run the search
#### with docker

```shell
docker build -t file-search:0.0.1 .
```

```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

docker run --rm \
  -v $SEARCH_PATH:$SEARCH_PATH:ro \
  file-search:0.0.1 "run $WORD $SEARCH_PATH"
```

#### with sbt
```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

sbt -warn "run $WORD $SEARCH_PATH"
```
---
### How to run the test suite 
#### with docker

```shell
docker run --rm file-search:0.0.1 test
```

#### with sbt
```shell
sbt test
```

### To debug the application (i.e. see log statements)
Add `LOG_LEVEL=DEBUG`
#### with docker 
```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

docker run --rm \
  -v $SEARCH_PATH:$SEARCH_PATH:ro \
  -e LOG_LEVEL=DEBUG \
  file-search:0.0.1 "run $WORD $SEARCH_PATH"
```

#### with sbt
```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

sbt -warn "run $WORD $SEARCH_PATH" -DLOG_LEVEL=DEBUG
```

### How to generate coverage reports
To run tests with coverage enabled and generate coverage report
```shell
sbt clean coverage test coverageReport
```

To check coverage report locally on a linux machine with Google Chrome installed:
```shell
google-chrome $(pwd)/target/scala-2.13/scoverage-report/index.html
```
---
### Application settings

#### Case-insensitive search

Case-insensitive search is the default when looking for the given word in the files

To make the search case-sensitive, set the environment variable `CASE_INSENSITIVE_SEARCH=false` 

##### with docker
```shell
WORD=hI
SEARCH_PATH=$HOME/Documents/

docker run --rm \
  -v $SEARCH_PATH:$SEARCH_PATH:ro \
  -e CASE_INSENSITIVE_SEARCH=false \
  file-search:0.0.1 "run $WORD $SEARCH_PATH"
```

##### with sbt
```shell
WORD=hI
SEARCH_PATH=$HOME/Documents/

sbt -warn "run $WORD $SEARCH_PATH" -DCASE_INSENSITIVE_SEARCH=false
```

#### Path max depth
`MAX_DEPTH` is the maximum number of levels of directories to visit. 

A value of `0` means that only the given path is visited.

```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

docker run --rm \
  -v $SEARCH_PATH:$SEARCH_PATH:ro \
  -e MAX_DEPTH=1 \
  file-search:0.0.1 "run $WORD $SEARCH_PATH"
```

##### with sbt
```shell
WORD=hi
SEARCH_PATH=$HOME/Documents/

sbt -warn "run $WORD $SEARCH_PATH" -DMAX_DEPTH=1
```
---
### Building native standalone executable with GraalVM
#### Use [sbt assembly](https://github.com/sbt/sbt-assembly) to create a fat jar
```shell
sbt clean assembly
# output: target/scala-2.13/FileSearch-assembly-0.1.0-SNAPSHOT.jar
```

#### Install [GraalVM](https://www.graalvm.org/22.3/reference-manual/native-image/)
```shell
sdk install java 22.3.r19-grl 
sdk default java 22.3.r19-grl
```

#### Build the native binary

```shell
native-image --verbose --no-fallback \
  -H:ResourceConfigurationFiles=META-INF/native-image/resource-config.json \
  -H:ReflectionConfigurationFiles=META-INF/native-image/reflect-config.json \
  -jar target/scala-2.13/FileSearch-assembly-0.1.0-SNAPSHOT.jar
```

Native Image Build Configuration `-H:...`
> `-H:ResourceConfigurationFiles`
> 
> By default, the `native-image` builder will not integrate any of the resources that are on the classpath into the native executable. (i.e. `application.conf`, `logback.xml`)
>
> see [Accessing Resources in Native Image](https://www.graalvm.org/22.3/reference-manual/native-image/dynamic-features/Resources/)
> 
> Adding `resource-config.json` to the `native-image` builder solves that issue 
> 

> `-H:ReflectionConfigurationFiles`
> 
> The `native-image` utility provides partial support for reflection. 
> It uses static analysis to detect the elements of your application that are accessed using the Java Reflection API. 
> 
> However, because the analysis is static, it cannot always completely predict all usages of the API when the program runs. 
> In these cases, you must provide a configuration file to the native-image utility to specify the program elements that use the API.
>
> see [Build a Native Executable with Reflection](https://www.graalvm.org/22.2/reference-manual/native-image/guides/build-with-reflection/)
> 
> Adding `reflect-config.json` to the `native-image` builder solves that issue

> GraalVM provides a `Tracing Agent` to easily gather metadata and prepare configuration files. 
> The agent tracks all usages of dynamic features during application execution on a regular Java VM.
>
> see [Collect Metadata with the Tracing Agent](https://www.graalvm.org/22.3/reference-manual/native-image/metadata/AutomaticMetadataCollection/#tracing-agent)
> 
> Enable the agent on the command line with the `java` command from the GraalVM JDK
> When the application completes and the JVM exits, the agent writes metadata to JSON files in the specified output directory
> ```shell
> $JAVA_HOME/bin/java -agentlib:native-image-agent=config-output-dir=META-INF/native-image -jar target/scala-2.13/FileSearch-assembly-0.1.0-SNAPSHOT.jar hi $HOME/Documents
> # Note that the output of the command contains more info than what is checked-in in git,
> # I only kept what is necessary to run the app without errors.
> ```

#### Run the file search using the native standalone executable
```shell
$(pwd)/FileSearch-assembly-0.1.0-SNAPSHOT -DLOG_LEVEL=INFO hi $HOME/Documents
```
---
### Use filesearch
Now that we have a native binary, we can copy it to local bin folder
```shell
sudo cp $(pwd)/FileSearch-assembly-0.1.0-SNAPSHOT /usr/local/bin/filesearch
```
To override application config and persist the changes between application runs
```shell
cat<<EOF > $HOME/.config/FileSearch.config
# lines that start with # are comments
max-depth = 10
file-content-types-whitelist = ["json"]
# case-insensitive-search = true
EOF
```

Run a search anywhere in your terminal
```shell
filesearch hi $HOME/Documents
```


