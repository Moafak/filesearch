#!/bin/bash

function fs() {
  WORD="$1"
  SEARCH_PATH="$2"
  if [ -z "$WORD" ] || [ -z "$SEARCH_PATH" ]; then
    echo "Requires two arguments: word, path"
    echo "Exiting..."
    read -p ""
    exit 1
  fi

  sbt -warn "run $WORD $SEARCH_PATH"
}

function fsd() {
    WORD="$1"
    SEARCH_PATH="$2"
    if [ -z "$WORD" ] || [ -z "$SEARCH_PATH" ]; then
      echo "Requires two arguments: word, path."
      echo "Exiting..."
      read -p ""
      exit 1
    fi

    docker run --rm \
          -v $SEARCH_PATH:$SEARCH_PATH:ro \
          "$(docker build --quiet .)" "run $WORD $SEARCH_PATH"
}