FROM sbtscala/scala-sbt:eclipse-temurin-17.0.5_8_1.8.2_2.13.10

COPY build.sbt /root/

RUN sbt update

COPY  src/ /root/src

RUN sbt compile test:compile

ENTRYPOINT ["sbt", "-warn"]

