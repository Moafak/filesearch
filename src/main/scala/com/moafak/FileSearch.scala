package com.moafak

import cats.effect.Async
import com.moafak.config.FileSearchConfig
import com.typesafe.scalalogging.Logger
import fs2.io.file.{Files, Path}
import fs2.text

import scala.util.{Success, Try}

final case class FileSearch[F[_]: Async](config: FileSearchConfig) {

  val logger = Logger(getClass.getName)

  private def pathIsReadable(p: Path): Either[PathIsNotReadable, Path] =
    Try(java.nio.file.Files.isReadable(p.toNioPath)) match {
      case Success(exists) if exists => Right(p)
      case _                         => Left(PathIsNotReadable(p.toString))
    }

  private def fileContentTypeFilter: fs2.Pipe[F, Path, Path] = in =>
    in
      .debug(p => s"$p -> content type: ${java.nio.file.Files.probeContentType(p.toNioPath)}", logger.debug(_))
      .map(p => p -> Try(java.nio.file.Files.probeContentType(p.toNioPath)).toOption.flatMap(Option(_)))
      .collect {
        case (p, Some(contentType)) if config.fileContentTypesWhitelist.exists(ct => contentType.contains(ct)) => p
      }

  private def fileContainsWordFilter(word: String): fs2.Pipe[F, Path, Path] =
    in =>
      in.flatMap { path =>
        Files[F]
          .readAll(path)
          .through(text.utf8.decode)
          .exists(str =>
            if (config.caseInsensitiveSearch) str.toLowerCase.contains(word.toLowerCase) else str.contains(word)
          )
          .map(condition => condition -> path)
          .debug(c_p => s"${c_p._2} -> contains word: ${c_p._1}", logger.debug(_))
          .collect { case (true, path) => path }
      }

  def pathIsReadable(p: String): Either[PathIsNotReadable, Path] =
    pathIsReadable(Path(p))

  def filesStream(word: String, path: String) =
    Files[F]
      .walk(Path(path), config.maxDepth, false)
      .filter(p => pathIsReadable(p).isRight)
      .debug(p => s"$p -> is directory: ${p.toNioPath.toFile.isDirectory}", logger.debug(_))
      .filter(p => p.toNioPath.toFile.isFile)
      .through(fileContentTypeFilter)
      .through(fileContainsWordFilter(word))

}
