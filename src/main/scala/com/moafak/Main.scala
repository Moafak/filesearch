package com.moafak

import cats.effect.{ExitCode, IO, IOApp}
import com.typesafe.scalalogging.Logger
object Main extends IOApp {
  val logger = Logger(getClass.getName)
  override def run(args: List[String]): IO[ExitCode] = {
    logger.debug(s"args: ${args.mkString(" ")}")

    args match {
      case word :: path :: Nil =>
        (for {
          fileSearchConfig <- config.loadConfig()
          fileSearch = new FileSearch[IO](fileSearchConfig)
          _ <- IO.fromEither(fileSearch.pathIsReadable(path))
          _ <- fileSearch
            .filesStream(word, path)
            .evalTap(p => IO.println(p))
            .compile
            .drain
        } yield ())
          .as(ExitCode.Success)
      case _ =>
        IO.raiseError(ArgumentsError(args)).as(ExitCode.Error)
    }
  }
}
