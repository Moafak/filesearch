package com.moafak

import cats.Show

sealed trait Validation extends Throwable with scala.util.control.NoStackTrace {
  override def getMessage: String = Show[Validation].show(this)
}

case class ArgumentsError[A](args: List[A]) extends Validation
case class ConfigReadError(message: String) extends Validation
case class PathIsNotReadable(path: String) extends Validation

object Validation {
  implicit val showValidation: Show[Validation] = Show[Validation] {
    case ArgumentsError(args)    => s"Expected 2 arguments, got ${args.mkString(", ")}"
    case ConfigReadError(msg)    => s"Error reading config: $msg"
    case PathIsNotReadable(path) => s"Path is not readable: $path"
  }
}
