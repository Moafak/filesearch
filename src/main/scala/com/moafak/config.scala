package com.moafak

import cats.effect.IO
import cats.syntax.either._
import pureconfig._
import pureconfig.generic.auto._

object config {
  case class FileSearchConfig(
      maxDepth: Int,
      fileContentTypesWhitelist: List[String],
      caseInsensitiveSearch: Boolean
  )

  def loadConfig(): IO[FileSearchConfig] =
    IO.fromEither(
      ConfigSource
        .file(new java.io.File(System.getProperty("user.home") + "/.config/FileSearch.config"))
        .optional
        .withFallback(
          ConfigSource.default
        )
        .load[FileSearchConfig]
        .leftMap(err => ConfigReadError(err.prettyPrint()))
    )
}
