package com.moafak

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.moafak.config.FileSearchConfig
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.wordspec.AnyWordSpec

import java.nio.file.Files

class FileSearchSpec extends AnyWordSpec with Matchers with BeforeAndAfterAll with Eventually {
  private var tempDir: java.nio.file.Path = _
  private val config: FileSearchConfig = FileSearchConfig(
    maxDepth = 3,
    fileContentTypesWhitelist = List("text"),
    caseInsensitiveSearch = true
  )

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    tempDir = Files.createTempDirectory("FileSearchTest")
  }

  override protected def afterAll(): Unit = {
    scala.reflect.io.Directory(tempDir.toFile).deleteRecursively()
    super.afterAll()
  }

  private def createFile(
      content: String,
      parents: String,
      name: String
  ): java.nio.file.Path = {
    val fileContent = content.getBytes
    val file1 = tempDir.resolve(parents).resolve(name)

    Files.createDirectories(tempDir.resolve(parents))
    Files.write(file1, fileContent)

    eventually(timeout(Span(2, Seconds)), interval(Span(5, Millis))) {
      Files.exists(file1) shouldBe true
    }

    file1
  }

  "file search" should {
    "pick files whose content type is in the whitelist" in {
      val word = "hello"
      val file1 =
        createFile("hello world! from csv file", "dir1/dir2", "file1.csv")
      val file2 =
        createFile("hello world! from txt file", "dir1/dir2", "file2.txt")
      val file3 = createFile("hello world! from json file", "dir1/dir2", "file3.json")

      val fileSearch = new FileSearch[IO](config)
      val filesContainingWord =
        fileSearch
          .filesStream(word, tempDir.toString)
          .compile
          .toList
          .unsafeRunSync()
          .map(_.toNioPath)

      filesContainingWord should contain allOf (file1, file2)
      filesContainingWord shouldNot contain(file3)

    }
    "not reach files whose depth exceeds config" in {
      val word = "hello"
      val file1 = createFile(
        "hello world! from csv file",
        "dir1/dir2/dir3/dir4",
        "file1.csv"
      )
      val file2 =
        createFile("hello world! from txt file", "dir1/dir2", "file2.txt")

      val fileSearch = new FileSearch[IO](config)
      val filesContainingWord =
        fileSearch
          .filesStream(word, tempDir.toString)
          .compile
          .toList
          .unsafeRunSync()
          .map(_.toNioPath)

      filesContainingWord shouldNot contain(file1)
      filesContainingWord should contain(file2)
    }
    "can be case sensitive" in {
      val word = "Hello"
      val file1 =
        createFile("hello world! from csv file", "dir1/dir2", "file1.csv")

      val fileSearch =
        new FileSearch[IO](config.copy(caseInsensitiveSearch = false))
      val filesContainingWord =
        fileSearch
          .filesStream(word, tempDir.toString)
          .compile
          .toList
          .unsafeRunSync()
          .map(_.toNioPath)

      filesContainingWord shouldBe Nil
    }
  }
}
