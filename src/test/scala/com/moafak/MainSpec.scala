package com.moafak

import cats.effect.ExitCode
import cats.effect.unsafe.implicits.global
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MainSpec extends AnyWordSpec with Matchers {
  "Main" should {
    "run when 2 arguments are provided - path not readable" in {
      Main
        .run(List("arg1", "arg2"))
        .attempt
        .unsafeRunSync() should matchPattern { case Left(PathIsNotReadable(_)) =>
      }
    }
    "run when 2 arguments are provided - path is readable" in {
      val existingPath = getClass.getResource("").getPath
      Main
        .run(List("arg1", existingPath))
        .attempt
        .unsafeRunSync() shouldBe Right(ExitCode.Success)
    }

    "exit otherwise" in {
      Main.run(Nil).attempt.unsafeRunSync() should matchPattern { case Left(ArgumentsError(_)) => }
      Main.run(List("arg1")).attempt.unsafeRunSync() should matchPattern { case Left(ArgumentsError(_)) => }
      Main
        .run(List("arg1", "arg2", "arg3"))
        .attempt
        .unsafeRunSync() should matchPattern { case Left(ArgumentsError(_)) => }
    }
  }
}
